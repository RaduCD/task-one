package com.example.task_one_android

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import kotlinx.android.synthetic.main.fragment_first.*

class FirstFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    private fun goToSessionActivity(){
        var intent: Intent = Intent(activity, SessionActivity::class.java)
        startActivity(intent)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        var view =  inflater.inflate(R.layout.fragment_first, container, false)
        var button = view.findViewById<Button>(R.id.btnGoToSessionActivity)

        button.setOnClickListener(View.OnClickListener {
          goToSessionActivity()
        })
        return view
    }


}
