package com.example.task_one_android

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.fragment.app.FragmentTransaction

class SessionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_session)

        goToFirstFragmentSessionActivity()
    }

    private fun goToFirstFragmentSessionActivity(){
        val firstFragmentSessionActivity = FirstFragmentSessionActivity()

        supportFragmentManager.beginTransaction().add(R.id.fragmentContainer, firstFragmentSessionActivity)
            .addToBackStack("FIRST FRAGMENT SESSION ACTIVITY").commit()
    }
}
