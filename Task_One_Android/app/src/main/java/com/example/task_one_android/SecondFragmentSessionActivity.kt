package com.example.task_one_android

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment

class SecondFragmentSessionActivity : Fragment() {


    private fun replaceWithThirdFragment(){
        var thirdFragmentSessionActivity = ThirdFragmentSessionActivity()
        activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, thirdFragmentSessionActivity).
        addToBackStack("THIRD FRAGMENT SESSION ACTIVITY").commit()

    }

    private fun removeFirstFragmentSessionActivity(){



                    val secondFragmentSessionActivity = SecondFragmentSessionActivity()
                    getFragmentManager()!!.beginTransaction().replace(R.id.fragmentContainer, secondFragmentSessionActivity)
                        .addToBackStack(null).commit();


        var fm = activity!!.supportFragmentManager;

        fm.findFragmentByTag("FIRST FRAGMENT SESSION ACTIVITY")?.let {
            fm.beginTransaction().remove(
                it
            ).commit()
        }


        }


    private fun closeTheSessionActivity(){
        activity!!.finish();
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val view = inflater.inflate(R.layout.fragment_second_session_activity, container, false)

        val buttonReplaceWithThirdFragmentSessionActivity = view.findViewById<Button>(R.id.btnReplaceWithThirdFragmentSessionActivity)
        val buttonRemoveFirstFragmentSessionActivity = view.findViewById<Button>(R.id.btnRemoveFirstFragmentSessionActivity)
        val buttonCloseTheActivity = view.findViewById<Button>(R.id.btnCloseTheActivity)


        buttonReplaceWithThirdFragmentSessionActivity.setOnClickListener ( View.OnClickListener {
            replaceWithThirdFragment()
        } )


        buttonRemoveFirstFragmentSessionActivity.setOnClickListener ( View.OnClickListener {
            removeFirstFragmentSessionActivity()
        } )


        buttonCloseTheActivity.setOnClickListener ( View.OnClickListener {
            closeTheSessionActivity()
        } )

        return view
    }

}
