package com.example.task_one_android

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.FragmentManager


class FirstFragmentSessionActivity : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }
    }

    private fun goToSecondFragmentSessionActivity() {

            var secondFragmentSessionActivity = SecondFragmentSessionActivity()
            activity!!.supportFragmentManager.beginTransaction().replace(R.id.fragmentContainer, secondFragmentSessionActivity).
            addToBackStack("SECOND FRAGMENT SESSION ACTIVITY").commit()

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_first_session_activity, container, false)

        val btnGoToSecondFragmentSA = view.findViewById<Button>(R.id.btnGoToSecondFragmentSessionActivity)
        btnGoToSecondFragmentSA.setOnClickListener(View.OnClickListener {
            goToSecondFragmentSessionActivity()
        })

        return view
    }

}
